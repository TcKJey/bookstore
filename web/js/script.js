/**
 * Created by Алексей on 18.02.16.
 */
function displayFilter() {
    var booksSelected =  $('#bookssearch-book_ids :selected');
    var authorsSelected =  $('#bookssearch-author_ids :selected');
    var shopsSelected =  $('#bookssearch-shop_ids :selected');

    $("#filter").text('');

    if ((booksSelected.length == 0) && (authorsSelected.length == 0) &&  (shopsSelected.length == 0)) {
        $("#filter").append("<p>");
        $("#filter").append("<b>Критерии фильтрации не заданы</b>");
        $("#filter").append("</p>");
        return;
    }

    if (booksSelected.length > 0) {
        $("#filter").append("<p>");
        $("#filter").append("<b>Книги:</b>");
        booksSelected.each(function(){
            $("#filter").append(" " + $(this).text() + ";");
        });
        $("#filter").append("</p>");
    }

    if (authorsSelected.length > 0) {
        $("#filter").append("<p>");
        $("#filter").append('<b>Авторы:</b>');
        authorsSelected.each(function(){
            $("#filter").append(" " + $(this).text() + ";");
        });
        $("#filter").append("</p>");
    }

    if (shopsSelected.length > 0) {
        $("#filter").append("<p>");
        $("#filter").append('<b>Магазины:</b>');
        shopsSelected.each(function(){
            $("#filter").append(" " + $(this).text() + ";");
        });
        $("#filter").append("</p>");
    }
}

function searhBooks() {
    $("#books-search-form").submit();
    $("#books-search").on("pjax:end", function() {
        $.pjax.reload({container:"#books-list"});
    });

    displayFilter();
}

var myTimer;

$(document).ready(function() {
    displayFilter();
});

$('body').on('change', '#bookssearch-book_ids', function(){
     clearTimeout(myTimer);
     myTimer = setTimeout(searhBooks, 1000);
});
$('body').on('change', '#bookssearch-author_ids', function(){
    clearTimeout(myTimer);
    myTimer = setTimeout(searhBooks, 1000);
});
$('body').on('change', '#bookssearch-shop_ids', function(){
    clearTimeout(myTimer);
    myTimer = setTimeout(searhBooks, 1000);
});
