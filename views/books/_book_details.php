<?php
use app\models\Books;
?>
    <p><b>Информация о книге</b></p>
    <p>
        <b>Название:</b>
        <?= $model->name ?>
    </p>
    <p><b>Автор: </b>
    <?php foreach ($model->authors as $author) {
        echo  $author->full_name . '; ';
    }?>
    </p>
    <p>
        <b>Год выпуска: </b>
        <?= $model->publish_year ?>
    </p>
    <p><b>Магазин:</b>
    <?php foreach ($model->shops as $shop) {
        echo  $shop->name . '; ';
    }?>
    </p>


