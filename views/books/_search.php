<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\BooksSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="books-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'id' => 'books-search-form',
        'options' => ['data-pjax' => true]
    ]); ?>
<div class="col-md-2">
    <?= $form->field($model, 'book_ids')->label('Книги')->listBox($books, [
        'multiple' => 'true',
        'size' => 10,
    ]) ?>
</div>
<div class="col-md-2">
    <?= $form->field($model, 'author_ids')->label('Авторы')->listBox($authors, [
        'multiple' => 'true',
        'size' => 10,
    ]) ?>
</div>
<div class="col-md-2">
    <?= $form->field($model, 'shop_ids')->label('Магазины')->listBox($shops, [
        'multiple' => 'true',
        'size' => 10,
    ]) ?>
</div>
<div class="clearfix"></div>
<?php ActiveForm::end(); ?>
</div>
