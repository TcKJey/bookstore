<?php

use yii\helpers\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BooksSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Склад книг';
?>
<div class="books-index">
    <?php Pjax::begin(['id' => 'books-search']); ?>
    <?php  echo $this->render('_search', ['model' => $searchModel,
        'books' => $books,
        'authors' => $authors,
        'shops' => $shops,
    ]); ?>
    <?php Pjax::end(); ?>
    <p>
        <h3>Витрина</h3>
    </p>
    <?php
    ?>
    <div id="filter"></div>
    <?php Pjax::begin(['id' => 'books-list']); ?>
    <?= ListView::widget([
    'dataProvider' => $dataProvider,
        'itemView' => '_book_details.php',
        'layout' => "{items}<div class='clearfix'></div>{pager}",
        'pager' => [
            'firstPageLabel' => 'Начало',
            'lastPageLabel' => 'Конец',
        ],
        'itemOptions' => [
            'tag' => 'div',
            'class' => 'col-md-2 well book-list-view',
        ],
        'emptyText' => 'Список пуст',
    ]);
    ?>
    <?php Pjax::end(); ?>
</div>
