<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "authors".
 *
 * @property string $id
 * @property string $full_name
 */
class Authors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name'], 'required'],
            [['full_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
        ];
    }

    public function getBooks()
    {
        return $this->hasMany(Books::className(), ['id' => 'book_id'])
            ->viaTable('authors_books', ['author_id' => 'id']);
    }
}