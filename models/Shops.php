<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "shops".
 *
 * @property string $shop_id
 * @property string $name
 * @property string $address
 * @property string $phone
 */
class Shops extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shops';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'address', 'phone'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['address'], 'string', 'max' => 512],
            [['phone'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'name' => 'Name',
            'address' => 'Address',
            'phone' => 'Phone',
        ];
    }

    public function getBooks()
    {
        return $this->hasMany(Books::className(), ['id' => 'book_id'])
            ->viaTable('shops_books', ['shop_id' => 'id']);
    }
}
