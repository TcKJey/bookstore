<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "books".
 *
 * @property string $id
 * @property string $name
 * @property integer $publish_year
 */
class Books extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'publish_year'], 'required'],
            [['publish_year'], 'integer'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'publish_year' => 'Publish Year',
        ];
    }

    public function getAuthors()
    {
        return $this->hasMany(Authors::className(), ['id' => 'author_id'])
            ->viaTable('authors_books', ['book_id' => 'id']);
    }

    public function getShops()
    {
        return $this->hasMany(Shops::className(), ['id' => 'shop_id'])
            ->viaTable('shops_books', ['book_id' => 'id']);
    }
}
